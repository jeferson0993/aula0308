package ifto.jeferson.pdm2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SegundaActivity extends AppCompatActivity {
    private String dadosJaSON;
    private ListView listView;
    private List<Estudante> lista;
    private ArrayAdapter<Estudante> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);
        dadosJaSON = getIntent().getStringExtra("dados");
        listView = findViewById(R.id.listViewDados);
        lista = consumirJSON();
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, lista);
        listView.setAdapter(adapter);

        /**
         * evento de capturar o item selecionado na lista.
         * */
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String itemDaLista = adapterView.getItemAtPosition(i).toString();
                Toast.makeText(getApplicationContext(), itemDaLista, Toast.LENGTH_SHORT).show();
            }
        });
    }
    private List<Estudante> consumirJSON() {
        List<Estudante> listaEstudantes = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(dadosJaSON);
            JSONArray jsonArray = jsonObject.getJSONArray("estudantes");
            for(int i=0;i<jsonArray.length();i++){
                JSONObject object = jsonArray.getJSONObject((i));
                Estudante e = new Estudante();
                e.setNome(object.getString("nomeEstudante"));
                e.setDisciplina(object.getString("disciplinaEstudante"));
                e.setNota(object.getInt("notaEstudante"));
                listaEstudantes.add(e);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listaEstudantes;
    }
}